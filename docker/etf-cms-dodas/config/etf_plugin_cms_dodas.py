import logging
import itertools
import requests
import urlparse
import xml.etree.ElementTree as ET

from ncgx.inventory import Hosts, Checks, Groups
from vofeed.api import VOFeed

log = logging.getLogger('ncgx')

CE_STATE_METRICS = ('org.sam.CONDOR-JobState-/cms/Role=lcgadmin',)

CE_METRICS = (
    'org.sam.CONDOR-JobSubmit-/cms/Role=lcgadmin',
    'org.cms.WN-analysis-/cms/Role=lcgadmin',
    'org.cms.WN-isolation-/cms/Role=lcgadmin',
    'org.cms.WN-basic-/cms/Role=lcgadmin',
    'org.cms.WN-cvmfs-/cms/Role=lcgadmin',
    'org.cms.WN-env-/cms/Role=lcgadmin',
    'org.cms.WN-frontier-/cms/Role=lcgadmin',
    'org.cms.WN-mc-/cms/Role=lcgadmin',
    'org.cms.WN-remotestageout-/cms/Role=lcgadmin',
    'org.cms.WN-squid-/cms/Role=lcgadmin',
    'org.cms.WN-xrootd-access-/cms/Role=lcgadmin',
    'org.cms.WN-xrootd-fallback-/cms/Role=lcgadmin')


def run(url):
    log.info("Processing vo feed: %s" % url)

    # Get services from the VO feed, i.e 
    # list of tuples (hostname, flavor, endpoint)
    feed = VOFeed(url)
    services = feed.get_services()

    # Add hosts, each tagged with corresponding flavors
    # creates /etc/ncgx/conf.d/generated_hosts.cfg
    h = Hosts()
    for service in services:
        h.add(service[0], tags=[service[1]])
    h.serialize()

    # Add host groups
    sites = feed.get_groups("CMS_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.iteritems():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()

    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()

    # DODAS CEs
    # Add corresponding metrics to tags
    # creates /etc/ncgx/conf.d/generated_checks.cfg
    c = Checks()
    # HT-CONDOR-CEs
    for service in services:   # special handling for HTCONDOR-CEs (no BDII)
        if service[1] == 'HTCONDOR-CE' and 'fnal' in service[0]:
            for m in CE_STATE_METRICS:
                c.add(m, hosts=(service[0],), params={'args': {'--resource': 'htcondor://%s' % service[0]}})
                c.add_all(CE_METRICS, hosts=(service[0],))

    for m in CE_STATE_METRICS:
        c.add(m, hosts=('T3_IT_Opportunistic.dodas',), params={'args': {'--resource': 'dodas://T3_IT_Opportunistic.dodas/nosched/nolrms/noqueue',
             '--jdl-ads': '\'+DESIRED_Sites=\"{0}\"_NL_+JOB_CMSSite=\"{0}\"\''.format('T3_IT_Opportunistic')}})
        c.add_all(CE_METRICS, hosts=('T3_IT_Opportunistic.dodas',))
    c.serialize()

    # Add host groups
    sites = feed.get_groups("CMS_Site")
    hg = Groups("host_groups")
    for site, hosts in sites.iteritems():
        for host in hosts:
            hg.add(site, host)
    hg.serialize()
