#!/usr/bin/perl
#
# Script to modify the SAM configuration to have an explicit list of SRM instances to test
#
# Usage: makesrmlist.pl sam_conf new_sam_conf serviceabbr vo
#

use File::Temp qw/tempfile tmpnam/;
use File::Copy qw/copy/;

$same = $ENV{"SAME_HOME"};
$sameconf = $ARGV[0];
$outconf = $ARGV[1];
$servicetype = "SRMv2";
$vo = "cms";

# Read list of PhEDEx endpoints
$endp = 'http://magini.web.cern.ch/magini/phedex-v2-endpoints.txt';
$ret = system("wget $endp -O /tmp/endpoints");
if ( $ret ) {
  warn "Error downloading endpoint list\n";
} else {
  open(FILE, "cat /tmp/endpoints | dos2unix |") or warn "Cannot read temporary file\n";
  while (<FILE>) {
    chomp;
    my ($se, $site) = split /\s+/, $_;
    push @ses, $se if ( !grep($se eq $_, @ses) );
  }
  close FILE;
  unlink('/tmp/endpoints');
}

# Parse the original configuration file and build filter to get the nodenames
open(CONF, $sameconf) or die "Cannot open $sameconf\n";
while (<CONF>) {
  chomp;
  if (/^common_filter=\"(.*)\"/) {
    $filter = $1;
  } elsif (/^${servicetype}_filter=\"(.*)\"/) {
    $filter .= " $1";
  }
}
close CONF;
$filter =~ s/%\(master_vo\)s/$vo/;
$newfilter = "common_filter=\"$filter nodename=";
foreach (@ses) {
  my $node = $_;
  chomp $node;
  $newfilter .= "$node,";
}
$newfilter =~ s/,$/\"/;
open(CONF, "$sameconf") or die "Cannot open $sameconf\n";
open(NEWCONF, "> $outconf") or die "Cannot open $outconf\n";
while (<CONF>) {
  chomp;
  if ( /^common_filter/ ) {
    $_ = $newfilter;
  }
  print NEWCONF "$_\n";
}
close NEWCONF;
close CONF;

