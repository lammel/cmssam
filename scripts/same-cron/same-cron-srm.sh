#!/bin/bash --login

# Source UI environment
source `dirname $0`/ui.sh
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: error in sourcing the environment"
  exit 1
fi

# Define location of the cron jobs
SAME_CRON=$HOME/same-cron

if [ -e $SAME_CRON/lock-srm ] ; then
  if [ $((`date +%s` - `stat -c %Y $SAME_CRON/lock-srm`)) -ge 21600 ] ; then
    /bin/echo "--- Sensor execution skipped, lock file present and older than 6 hours"
  fi
  exit 0
fi
/bin/touch $SAME_CRON/lock-srm
if [ $? -ne 0 ] ; then
  /bin/echo "--- FATAL: could not create lock file"
  exit 1
fi

# Define location of proxy
TMPFILE=`mktemp`
if [ $? -ne 0 ] ; then
  /bin/rm -f $SAME_CRON/lock-srm
  /bin/echo "--- FATAL: could not create temp file"
  exit 1
fi

#Define the location of SAM
SAME_DIR=$HOME/same-prod

#Define working directory for this SAM instance. The actual directory will be $HOME/.$SAME_WORKDIR
SAME_WORKDIR=`basename $SAME_DIR`

# Define which sensors to run and to publish
sensors="SRMv2"

/bin/echo -n "STARTTIME: "
/bin/date "+%Y-%m-%d %H:%M:%S"

# Update CMS scripts from CVS (comment if you do not want automatic updates)
$SAME_CRON/cms2sam.sh $SAME_DIR 2>&1

# Uncomment if you are running with the /cms/Role=production FQAN
# Blacklist specific SRM instances
export SAME_HOME=$SAME_DIR/client

$SAME_CRON/makesrmlist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same_SRMv2.conf 2>&1
export SAME_WORK=$HOME/.same-prod

echo ""
echo ""
echo -n "----------------------[ "
date

# Define the location of your certificate
export X509_USER_CERT=/afs/cern.ch/user/s/samcms/.globus-sciaba/usercert.pem
export X509_USER_KEY=/afs/cern.ch/user/s/samcms/.globus-sciaba/userkey.pem
export X509_USER_PROXY=$TMPFILE

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=production'
lifetime='24:00'

echo "+++ Using the $fqan FQAN"

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < /afs/cern.ch/user/s/samcms/.pawlogin 2>&1
if [ $? != 0 ] ; then
  /bin/echo "--- FATAL: proxy creation failed!"
  /bin/rm -f $SAME_CRON/lock-srm
  exit 1
fi  

for sensor in $sensors ; do
/bin/echo ""
/bin/echo "----------"
/bin/echo "Executing $sensor sensor: "
/bin/echo ""
$SAME_DIR/client/bin/same-exec -c $SAME_DIR/client/etc/same_SRMv2.conf $sensor 2>&1
done

/bin/echo -n "ENDTIME: "
/bin/date "+%Y-%m-%d %H:%M:%S"

/bin/rm -f $TMPFILE
/bin/rm -f $SAME_CRON/lock-srm
