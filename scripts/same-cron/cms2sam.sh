#!/bin/bash
#
# Script to update the SAM installation with the latest CMS tests from CVS
#
SAME_DIR=$1
tmpdir=`mktemp -d`
export CVSROOT=:pserver:anonymous@cmscvs.cern.ch:/cvs_server/repositories/CMSSW
cd $tmpdir
rm -rf CMSSAM
cvs co CMSSAM > /dev/null
res=$?
if [ $res != 0 ] ; then
   echo "CVS checkout failed"
   exit 1
fi
CMS_DIR=$tmpdir/CMSSAM

# Copy configuration file
cp $CMS_DIR/config/same.conf.CMS $SAME_DIR/client/etc

# testjob sensor
sensor='testjob'
modules='FroNtier MonteCarlo testjob'
for i in $modules ; do
   cd $CMS_DIR/SiteTests/$i
   for j in `find . -not -regex '.*\(CVS\).*'` ; do
      if [ -d $j ] ; then
         mkdir -p $SAME_DIR/client/sensors/$sensor/$j
      else
         cp $j $SAME_DIR/client/sensors/$sensor/`dirname $j`
      fi
   done
done

# cream-testjob sensor
mkdir -p $SAME_DIR/client/sensors/cream-testjob
cp -a $SAME_DIR/client/sensors/testjob/* $SAME_DIR/client/sensors/cream-testjob/
cd $SAME_DIR/client/sensors/cream-testjob
for i in *.lst ; do
   sed -e 's/^CE/CREAMCE/' $i > tmp
   mv tmp $i
done 
cd tests
for old in CE-cms-* ; do
   new=`echo $old | sed -e 's/^CE/CREAMCE/'`
   mv $old $new
done
for i in CREAMCE-cms*.def ; do
   sed -e 's/testName: CE/testName: CREAMCE/' $i > tmp
   mv tmp $i
done

# Other sensors
sensors='CE SRM SRMv2'
for sensor in $sensors ; do
   cd $CMS_DIR/SiteTests/$sensor
   for j in `find . -not -regex '.*\(CVS\).*'` ; do
      if [ -d $j ] ; then
         mkdir -p $SAME_DIR/client/sensors/$sensor/$j
      else
         cp $j $SAME_DIR/client/sensors/$sensor/`dirname $j`
      fi
   done
done

rm -rf $tmpdir
