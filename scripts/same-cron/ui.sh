source /afs/cern.ch/project/gd/LCG-share/3.2.4-0/etc/profile.d/grid_env.sh
export LCG_GFAL_INFOSYS=sam-bdii.cern.ch:2170
export GLITE_WMS_CLIENT_CONFIG=/afs/cern.ch/cms/LCG/LCG-2/UI/conf/glite_wms_SAM.conf
export GLITE_WMSUI_COMMANDS_CONFIG=/afs/cern.ch/cms/LCG/LCG-2/UI/conf/glite_wms_SAM.conf
export HOME=/data/sam2
export PATH=$PATH:/data/sam2/bin
