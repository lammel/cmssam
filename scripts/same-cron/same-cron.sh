#!/bin/bash --login

# Define the Myproxy server to use
myproxy=myproxy-fts.cern.ch

# Define location of the cron jobs
SAME_CRON=$HOME/same-cron

# Define location of proxy
TMPFILE=`mktemp`
if [ $? -ne 0 ] ; then
  echo "Failed to create temporary file. Exiting..."
  exit 1
fi
export X509_USER_PROXY=$TMPFILE

# START of block to duplicate to add new SAM instances

# Define the location of the log file
log=$HOME/same-cron/same-cron.log

#Define the location of SAM
SAME_DIR=$HOME/same

#Define working directory for this SAM instance. The actual directory will be $HOME/.$SAME_WORKDIR
SAME_WORKDIR=`basename $SAME_DIR`

# Define which sensors to run and to publish
publish_sensors="CE"
sensors="CE"

echo -n "STARTTIME: " >> $log
date >> $log

# Update CMS scripts from CVS (comment if you do not want automatic updates)
$SAME_CRON/cms2sam.sh $SAME_DIR >> $log 2>&1

# Update list of CMS sites to test (comment if you do not want automatic updates)
$SAME_CRON/makesitelist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same.conf $SAME_WORKDIR >> $log 2>&1

# Blacklist specific CE instances
export SAME_HOME=$SAME_DIR/client
$SAME_CRON/makeservicelist.pl $SAME_DIR/client/etc/same.conf $SAME_DIR/client/etc/same_CE.conf CE cms >> $log 2>&1

# Uncomment if you are running with the /cms/Role=production FQAN
# Blacklist specific SRM instances
#export SAME_HOME=$SAME_DIR/client
#$SAME_CRON/makeservicelist.pl $SAME_DIR/client/etc/same.conf $SAME_DIR/client/etc/same_SRM.conf SRM cms >> $log 2>&1

# Uncomment if you are running with the /cms/Role=production FQAN
#cp -f $SAME_DIR/client/sensors/CE/tests/CE-sft-job $SAME_DIR/client/sensors/CE/tests/CE-cms-prod >> $log 2>&1
#cp -f $SAME_DIR/client/sensors/CE/test-sequence-prod.lst $SAME_DIR/client/sensors/CE/test-sequence.lst >> $log 2>&1
#cp -f $SAME_DIR/client/sensors/CE/config-prod.sh $SAME_DIR/client/sensors/CE/config.sh >> $log 2>&1
#cp -f $SAME_DIR/client/sensors/testjob/test-sequence-prod.lst $SAME_DIR/client/sensors/testjob/test-sequence.lst

echo "" >> $log
echo "" >> $log
echo -n "----------------------[ " >> $log
date >> $log


# Define the location of your certificate
export X509_USER_CERT=$HOME/.globus-asciaba/usercert.pem
export X509_USER_KEY=$HOME/.globus-asciaba/userkey.pem

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=lcgadmin'
lifetime='24:00'

echo "+++ Using the $fqan FQAN" >> $log

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < $HOME/.grid-cert-passphrase >> $log 2>&1
if [ $? != 0 ] ; then
  echo "Proxy creation failed!" >> $log
  publish_sensors=""
  sensors=""
fi  

for sensor in $publish_sensors ; do
echo "" >> $log	
echo "----------" >> $log
echo "Publishing $sensor sensor: " >> $log
echo "" >> $log
$SAME_DIR/client/bin/same-exec -c $CONF --publish $sensor >> $log 2>&1
done

for sensor in $sensors ; do
echo "" >> $log	
echo "----------" >> $log
echo "Executing $sensor sensor: " >> $log
echo "" >> $log
CONF=$SAME_DIR/client/etc/same.conf
if [ $sensor == 'CE' ] ; then
  CONF=$SAME_DIR/client/etc/same_CE.conf
fi
$SAME_DIR/client/bin/same-exec -c $CONF $sensor >> $log 2>&1
done

echo -n "ENDTIME: " >> $log
date >> $log

# end of block to duplicate to add new SAM instances
# START of block to duplicate to add new SAM instances

# Define the location of the log file
log=$HOME/same-cron/same-prod-cron.log

if [ -e $SAME_CRON/lock ] ; then
  echo "Sensor execution skipped, lock file present" >> $log
  exit 0
fi

#Create lock file
touch $SAME_CRON/lock

#Define the location of SAM
SAME_DIR=$HOME/same-prod

#Define working directory for this SAM instance. The actual directory will be $HOME/.$SAME_WORKDIR

SAME_WORKDIR=`basename $SAME_DIR`

# Define which sensors to run and to publish
publish_sensors="CE"
sensors="CE SRM SRMv2"

echo -n "STARTTIME: " >> $log
date >> $log

# Update CMS scripts from CVS (comment if you do not want automatic updates)
$SAME_CRON/cms2sam.sh $SAME_DIR >> $log 2>&1

# Update list of CMS sites to test (comment if you do not want automatic updates)
$SAME_CRON/makesitelist.pl $SAME_DIR/client/etc/same.conf.CMS $SAME_DIR/client/etc/same.conf $SAME_WORKDIR >> $log 2>&1

# Blacklist specific CE instances
export SAME_HOME=$SAME_DIR/client
$SAME_CRON/makeservicelist.pl $SAME_DIR/client/etc/same.conf $SAME_DIR/client/etc/same_CE.conf CE cms >> $log 2>&1

# Uncomment if you are running with the /cms/Role=production FQAN
# Blacklist specific SRM instances
export SAME_HOME=$SAME_DIR/client
$SAME_CRON/makeservicelist.pl $SAME_DIR/client/etc/same.conf $SAME_DIR/client/etc/same_SRM.conf SRM cms >> $log 2>&1

# Uncomment if you are running with the /cms/Role=production FQAN
cp -f $SAME_DIR/client/sensors/CE/tests/CE-sft-job $SAME_DIR/client/sensors/CE/tests/CE-cms-prod >> $log 2>&1
cp -f $SAME_DIR/client/sensors/CE/test-sequence-prod.lst $SAME_DIR/client/sensors/CE/test-sequence.lst >> $log 2>&1
cp -f $SAME_DIR/client/sensors/CE/config-prod.sh $SAME_DIR/client/sensors/CE/config.sh >> $log 2>&1
cp -f $SAME_DIR/client/sensors/testjob/test-sequence-prod.lst $SAME_DIR/client/sensors/testjob/test-sequence.lst >> $log 2>&1

echo "" >> $log
echo "" >> $log
echo -n "----------------------[ " >> $log
date >> $log


# Define the location of your certificate
export X509_USER_CERT=$HOME/.globus-newca/usercert.pem
export X509_USER_KEY=$HOME/.globus-newca/userkey.pem

# Choose an FQAN and a lifetime for the VOMS extensions
fqan='/cms/Role=production'
lifetime='48:00'

echo "+++ Using the $fqan FQAN" >> $log

voms-proxy-destroy
voms-proxy-init -debug -valid $lifetime -voms cms:$fqan -pwstdin < $HOME/.grid-cert-passphrase >> $log 2>&1
if [ $? != 0 ] ; then
  echo "Proxy creation failed!" >> $log
  publish_sensors=""
  sensors=""
fi

for sensor in $publish_sensors ; do
echo "" >> $log
echo "----------" >> $log
echo "Publishing $sensor sensor: " >> $log
echo "" >> $log
$SAME_DIR/client/bin/same-exec --publish $sensor >> $log 2>&1
done

for sensor in $sensors ; do
  echo "" >> $log
  echo "----------" >> $log
  echo "Executing $sensor sensor: " >> $log
  echo "" >> $log
  CONF=$SAME_DIR/client/etc/same.conf
  if [ $sensor == 'SRM' ] ; then
    CONF=$SAME_DIR/client/etc/same_SRM.conf
  fi
  if [ $sensor == 'CE' ] ; then
    CONF=$SAME_DIR/client/etc/same_CE.conf
  fi
  $SAME_DIR/client/bin/same-exec -c $CONF $sensor >> $log 2>&1
done
rm -f $SAME_CRON/lock

# Preliminary modification for SRM tests
#echo "" >> $log
#echo "----------" >> $log
#echo "Executing SRM sensor: " >> $log
#echo "" >> $log
#$SAME_DIR/client/bin/same-exec -c $HOME/same-srm.conf SRM >> $log 2>&1

echo -n "ENDTIME: " >> $log
date >> $log

# end of block to duplicate to add new SAM instances

rm -f $TMPFILE
