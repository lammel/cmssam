#!/usr/bin/env python
__version__ = "$Version$"
# $Id: __init__.py,v 1.4 2008/10/22 15:55:30 hernan Exp $


"""
_IMProv_

*In-memory Metadata and PROVenance toolkit*

Init module for IMProv package containing
classes for managing metadata and provenance information
for configurators using python XML DOM objects
"""

__all__ = ['IMProv']
